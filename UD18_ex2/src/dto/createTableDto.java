package dto;

public class createTableDto {
	
	private String nombreDB = "";
	private String nombreTabla = "";
	
	
	public createTableDto(DatabaseDto nombre, String nombreTabla) {
		super();
		this.nombreDB = DatabaseDto.nombre;
		this.nombreTabla = nombreTabla;
	}


	public String getNombreDB() {
		return nombreDB;
	}


	public void setNombreDB(String nombreDB) {
		this.nombreDB = nombreDB;
	}


	public String getNombreTabla() {
		return nombreTabla;
	}


	public void setNombreTabla(String nombreTabla) {
		this.nombreTabla = nombreTabla;
	}
		
}
