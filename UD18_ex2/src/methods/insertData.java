package methods;

import java.sql.SQLException;
import java.sql.Statement;

import dto.DatabaseDto;
import dto.createTableDto;

public class insertData {
	
	public static void insertData(DatabaseDto name, String nameTabla, String insertQuery) {
		
		try {
			String Querydb = "USE " + name.getNombre()+";";
			Statement stdb = openConnection.conexion.createStatement();
			stdb.executeLargeUpdate(Querydb);
			
			String Query = "INSERT INTO " + nameTabla + insertQuery;
			
			Statement st = openConnection.conexion.createStatement();
			st.executeUpdate(Query);
			
			System.out.println("Datos almacenados correctamente");
			
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Error en el almacenamiento");
		}

	}
	


}
