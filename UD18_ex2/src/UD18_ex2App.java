import java.sql.Connection;

import dto.DatabaseDto;
import dto.createTableDto;
import methods.closeConnection;
import methods.createDB;
import methods.createTable;
import methods.insertData;
import methods.openConnection;

public class UD18_ex2App {
	
	public static Connection conexion;

	public static void main(String[] args) {
	
        DatabaseDto newDatabase = new DatabaseDto("ejercicio2");
        
        createTableDto departamentos = new createTableDto(newDatabase, "departamentos");
        createTableDto EMPLEADOS = new createTableDto(newDatabase, "EMPLEADOS");
        
        String CreacionTabla1 = "CREATE TABLE `departamentos` (`Codigo` int NOT NULL,`Nombre` varchar(100) DEFAULT NULL,`Presupuesto` int DEFAULT NULL,PRIMARY KEY (`Codigo`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
        String CreacionTabla2 = "CREATE TABLE `EMPLEADOS` (`DNI` varchar(8) NOT NULL,`Nombre` varchar(100) DEFAULT NULL,`Apellidos` varchar(255) DEFAULT NULL,`Departamento` int DEFAULT NULL,PRIMARY KEY (`DNI`),KEY `Departamento_idx` (`Departamento`),CONSTRAINT `Departamento` FOREIGN KEY (`Departamento`) REFERENCES `departamentos` (`Codigo`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci";
        
        openConnection.openConnection();
        createDB.createDB(newDatabase);
        createTable.createTable(newDatabase, departamentos, CreacionTabla1);
        createTable.createTable(newDatabase, EMPLEADOS, CreacionTabla2);
        insertData.insertData(newDatabase, "departamentos", "(Codigo, Nombre, Presupuesto) VALUE(88, 'manel', 98);");
        closeConnection.closeConnection();
	}

}
